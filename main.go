// Stronghold Texture Tool
// Thanks to: MaxxLu, elfofthefire
// Web:	http://strongholdmods.forumphp3.net
//		http://stronghold.wikia.com
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"path"
)

var (
	op           string
	param        string
	verbose      bool
	build        bool
	buildTileset bool
	set          int
	param2		 string
)

func init() {
	flag.StringVar(&op, "op", "dir", "directory or file operation (values: dir, file, injectCollection)")
	flag.StringVar(&param, "i", "./input", "directory or file to convert")
	flag.BoolVar(&verbose, "verbose", false, "show debug info (values: true, false)")
	flag.BoolVar(&build, "build", true, "build collections (values: true, false)")
	flag.BoolVar(&buildTileset, "buildTileSet", true, "build tileset (values: true, false)")
	flag.IntVar(&set, "set", 0, "gm1 set i.e. teamcolor (values: 0 to 9). Colorset 0 for civilians.")
	flag.StringVar(&param2, "i2", "./input", "directory or file to process")
}

func main() {
	fmt.Println("TGX/GM1 Converter")

	flag.Parse()
/*	op = "injectCollection"
	param2 = "./input/tile_ruins.gm1"
	param = "./output_t/tile/tile_ruins.gm1/collection28.png"
	verbose=true*/
	
	switch op {
	case "dir":
		//convert all files in dir
		info, err := ioutil.ReadDir(param)
		if err != nil {
			log.Fatal("main: Cannot open dir: " + param)
		}

		for _, e := range info {
			if !e.IsDir() {
				filepath := param + "/" + e.Name()
				ext := path.Ext(filepath)
				switch ext {
				case ".tgx":
					var tgx TGXImage
					tgx.Load(filepath)
					tgx.Save()
				case ".gm1":
					var gm1 GM1Image
					gm1.Load(filepath)
					gm1.SmartSave()
				case ".png":
					
				}

			}
		}

	case "file":
		//convert a single file
		ext := path.Ext(param)
		switch ext {
		case ".tgx":
			var tgx TGXImage
			tgx.Load(param)
			tgx.Save()
		case ".gm1":
			var gm1 GM1Image
			gm1.Load(param)
			gm1.SmartSave()
		}
	case "injectCollection":
		//convert a single file
		ext := path.Ext(param2)
		switch ext {
		case ".tgx":
			var tgx TGXImage
			tgx.Load(param2)
			//tgx.Save()
		case ".gm1":
			var gm1 GM1Image
			gm1.Load(param2)
			log.Println(param2 + " loaded")
			gm1.MakeCollections()
			log.Println(param2 + " collections")
			//gm1.InjectCollection(param)
			log.Println(param + " injected")
			gm1.SaveNew()
		}
	}

}
