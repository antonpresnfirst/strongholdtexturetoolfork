package main

import (
	"encoding/binary"
	"bytes"
	"image"
	"image/color"
	"image/jpeg"
	"image/png"
	"image/draw"
	"golang.org/x/image/bmp"
	"io"
	"log"
	"os"
	"math"
	"fmt"
)

type SeekingReader interface {
	io.Reader
	io.Seeker
}


type SeekingWriter interface {
	io.Writer
	io.Seeker
}

type Image struct {
	data *image.RGBA
}

func (this *Image) Init(width, height int) {
	this.data = image.NewRGBA(image.Rect(0, 0, width, height))
}

func (this *Image) SetPixel(x, y int, r, g, b byte) {
	this.data.Set(x, y, color.RGBA{R: r, G: g, B: b, A: 255})
}

func (this *Image) Save(path, format string) {
	if this.data == nil {
		return
	}

	file, err := os.Create(path + "." + format)
	defer file.Close()
	if err != nil {
		log.Fatal("Save: unable to open file ", err)
	}
	switch format {
	case "jpg":
		jpeg.Encode(file, this.data, nil)
	case "png":
		png.Encode(file, this.data)
	case "bmp":
		bmp.Encode(file, this.data)
	}
}

func (this *Image) DecodeTGX(header TGXHeader, offset, size int64, reader SeekingReader, palette *TGXPalette) {
	this.Init(int(header.Width), int(header.Height))

	var token TGXToken
	var x, y int
	var r, g, b byte
	var pixelData uint16
	// var r_, g_ , b_ byte
	// r_ = 255
	// g_ = 0
	// b_ = 255
	
	reader.Seek(offset, 0)
	read := int64(0)

	for binary.Read(reader, binary.LittleEndian, &token) == nil && read < size {
		switch token.GetType() {
		case TGX_STREAM:
			logVerbose("Token: Stream, Length:", token.GetLength())

			for i := 0; i < token.GetLength(); i++ {
				if palette == nil {
					binary.Read(reader, binary.LittleEndian, &pixelData)
				} else {
					var index byte
					binary.Read(reader, binary.LittleEndian, &index)
					pixelData = palette[set*256+int(index)]
				}
				r, g, b = extractColors(pixelData)
				this.SetPixel(x, y, r, g, b)
				x++
			}

		case TGX_REPEAT:
			logVerbose("Token: Repeat, Length:", token.GetLength())

			if palette == nil {
				binary.Read(reader, binary.LittleEndian, &pixelData)
			} else {
				var index byte
				binary.Read(reader, binary.LittleEndian, &index)
				pixelData = palette[set*256+int(index)]
			}
			r, g, b = extractColors(pixelData)
			for i := 0; i < token.GetLength(); i++ {
				this.SetPixel(x, y, r, g, b)
				x++
			}

		case TGX_TRANSPARENT:
			logVerbose("Token: Transparent, Length:", token.GetLength())
		
			// for i:= 0; i<token.GetLength();i++{
				// this.SetPixel(i+x,y, r_,g_,b_)//set other pixels on line to default color
			// }
			x += token.GetLength()

		case TGX_NEWLINE:
			logVerbose(fmt.Sprintf("Token: New Line: %d\n",y))
			y++
			x = 0 //x -= int(header.Width)

		default:
			log.Fatal("ERROR: Unknown token")
		}
		currentPos, _ := reader.Seek(0, 1)
		read = currentPos - offset
	}
}

func (this * Image) EncodeTGX(writer SeekingWriter,palette *TGXPalette, maxX, maxY int) (offset uint32, size uint32){
	
	var r,g,b,a uint32
	
	// var color Color
	
	count := 0
	prev_count := count
	
	var prev,cur *int
	var prev_pixel, pixel *uint16
	var token TGXToken
	
	pixel = new (uint16)
	cur = new (int)
	// init buffer
	buffer := new (bytes.Buffer)
	// we need to limit repeat
	count_repeat := 0 
	limit_repeat := 2
	
	
	offset_before,_ := writer.Seek(0,1)
	for y:=0; y <= maxY-2; y++{
		
	
		// init
		prev = nil
		prev_pixel = nil
		
		count = 0
		prev_count = count
		count_repeat = 0

		//var tmp uint16
		for x:=0; x<=maxX; x++{
			
			// get current pixel
			r,g,b,a = this.data.At(x,y).RGBA()
			*pixel = setColors(r,g,b)
			
			if (a<255) {
				*cur = TGX_TRANSPARENT				
			} else {
				if ((prev_pixel != nil) && (*prev_pixel == *pixel)) {
					
					count_repeat++					

				} 
				
				if ((prev_pixel != nil) && (*prev_pixel == *pixel) && (count_repeat >= limit_repeat)) {
							
					*cur = TGX_REPEAT									
				} else {
					*cur = TGX_STREAM
					//logVerbose("pixel (r,g,b) = v",r,g,b,*pixel)
					binary.Write(buffer, binary.LittleEndian, pixel)
					//tmp = setColors(66,225,17)
					
					//binary.Write(buffer, binary.LittleEndian, tmp)

				}
			}
		
			// if stream type changes then write and reset
			if ((prev!=nil) && (*cur != *prev)) {
				// remember prev count and reset count				
				prev_count = count
				
				count = 0
				
				// increase repeat counter because we write last byte from buffer
				// decrease stream counter because it goes to repeat 
				if (*cur == TGX_REPEAT) {
					prev_count--
					count++
				}				
				
				// write flag and count 
				
				token.reset()
				token.SetType(*prev)
				token.SetLength(prev_count)
				binary.Write(writer, binary.LittleEndian, token)
				
				//logVerbose("write token ",token)
				
				if (*prev == TGX_STREAM) {
					logVerbose("write stream: length",prev_count)
				} else if (*prev == TGX_TRANSPARENT) {
					logVerbose("write transparent: length",prev_count)
				} else if (*prev == TGX_REPEAT) {
					logVerbose("write repeat: length",prev_count)
				}
				
				// here write stream or repeat pixel
				// each time we write TGX_STREAM or TGX_REPEAT we reset buffer
				if (*prev == TGX_STREAM) {					
					//logVerbose("buffer: ",buffer.Bytes())
					if (*cur == TGX_REPEAT) {
						// truncate buffer because last 2 bytes goes to repeat pixel
						if buffer.Len() >= (prev_count)*2 {
							buffer.Truncate((prev_count)*2)
						}	
					}
					buffer.WriteTo(writer)
					
					buffer.Reset()
				} else if (*prev == TGX_REPEAT) {
					logVerbose("repeat: pix = prev_pix",*pixel, *prev_pixel)
					binary.Write(writer, binary.LittleEndian, prev_pixel)
					
					buffer.Reset()
					count_repeat = 0
				}
			}
			
			// increase counter of stream bytes
			count++
			
			// set previous
			
			if (prev == nil) {
				prev = new (int)
			}
			
			if (prev_pixel == nil) {
				prev_pixel = new (uint16)
			}
			*prev = *cur			
			*prev_pixel = *pixel
		}
		
		// last pixel in a row
		
	
		// remember prev count and reset count				
		count--
		// write flag and count
		
		token.reset()
		token.SetType(*cur)
		token.SetLength(count)
		binary.Write(writer, binary.LittleEndian, token)
			
		
		//logVerbose("write token ",token)
		
		if (*cur == TGX_STREAM) {
			logVerbose("write stream: length",count)
		} else if (*cur== TGX_TRANSPARENT) {
			logVerbose("write transparent: length",count)
		} else if (*cur == TGX_REPEAT) {
			logVerbose("write repeat: length",count)
		}
		
		// here write stream or repeat pixel
		// each time we write TGX_STREAM or TGX_REPEAT we reset buffer
		if (*cur == TGX_STREAM) {					
			buffer.WriteTo(writer)
		
			buffer.Reset()
		} 
		
		// write new line if it is not first line
	
		token.reset()
		token.SetType(TGX_NEWLINE)
			
		binary.Write(writer, binary.LittleEndian, token)
		
		logVerbose("write newline token, y:",token, y)	
		
		
	}	
	// write 2 last lines always empty because of overlaping them by tile middle pixels
	for y:=maxY-1; y <= maxY; y++{
		token.SetType(TGX_NEWLINE)
			
		binary.Write(writer, binary.LittleEndian, token)
		
		logVerbose("write last newline token, y:",token, y)	
	}
	// bytes written
	offset_after, _ := writer.Seek(0,1)
	size = uint32(offset_after - offset_before)
	offset = uint32(offset_before)
	return
}

func (this *Image) DecodeBitmap(header GM1ImageHeader, offset, size int64, reader SeekingReader) {
	reader.Seek(offset, 0)

	this.Init(int(header.Width), int(header.Height)-7)
	rawPixels := make([]uint16, size)
	binary.Read(reader, binary.LittleEndian, &rawPixels)
	//put pixels into image
	var x, y int
	for _, u := range rawPixels {
		r, g, b := extractColors(u)
		this.SetPixel(x, y, r, g, b)
		x++
		if x >= int(header.Width) {
			x = 0
			y++
		}
	}
}

func (this *Image) DecodeTile(offset int64, reader SeekingReader) {
	reader.Seek(offset, 0)

	this.Init(GM1TILEWIDTH, GM1TILEHEIGHT)
	rawPixels := make([]uint16, GM1TILEBYTES/2)
	binary.Read(reader, binary.LittleEndian, &rawPixels)
	//put pixels into image
	var x, y int
		// var r_, g_ , b_ byte
// var i int
	// r_ = 250
	// g_ = 154
	// b_ = 185
	
	for _, u := range rawPixels {
		r, g, b := extractColors(u)
		
		this.SetPixel(15-GM1TilePixelsPerLine[y]/2+x, y, r, g, b) //diamond pattern
		x++
		if x >= GM1TilePixelsPerLine[y] {
			// for i= 0; i<(15 - GM1TilePixelsPerLine[y]/2);i++{
				// this.SetPixel(i,y, r_,g_,b_)//set other pixels on line to default color
			// }
			// for i= 15 - GM1TilePixelsPerLine[y]/2 + x; i<(30);i++{
				// this.SetPixel(i,y, r_,g_,b_)//set other pixels on line to default color
			// }
			x = 0			
			y++			
		}
	}
}

func (this * Image) EncodeTile(writer io.Writer){
	x:=0
	y:=0
	i:= 0
	var r,g,b uint32
	for _, u := range GM1TilePixelsPerLine{
		x = 15-u/2
		i = 0
		for ;i<u;{
			r, g, b, _ = this.data.At(x+i,y).RGBA()
			binary.Write(writer, binary.LittleEndian, setColors(r,g,b))			
			i++			
		}
		y++
	}
}

func (this *Image) ReplaceTile(img image.Image, header *GM1ImageHeader)(newSize uint32) {
	// type GM1ImageHeader struct {
	// Width            uint16
	// Height           uint16
	// PositionX        uint16
	// PositionY        uint16
	// Part             byte   //tile only
	// Parts            byte   //tile only
	// TilePositionY    uint16 //tile only
	// Direction        byte
	// HorizontalOffset byte //tile only
	// DrawingBoxWdith  byte //tile only
	// PerformanceID    byte
	// part :=header.Part
	parts :=header.Parts
	b:= img.Bounds()
	
	h:= b.Max.Y - b.Min.Y
	edge := int(math.Sqrt(float64(parts)))
	
	row, col, _ := header.getTileRowColDir(false)
	
	y := h - row*GM1TILEHEIGHT
	x := (int(int(math.Abs(float64(edge-row)))/2)+ col)*GM1TILEWIDTH
	
	rect := image.Rect(x, y, GM1TILEWIDTH+x, GM1TILEHEIGHT+y)
	var p image.Point
	p.X = 0
	p.Y = 0
	draw.DrawMask(this.data, rect, img, image.ZP, &tileMask{p}, image.ZP, draw.Over)
	//draw.Draw(this, rect, tileImg.data, image.ZP, draw.Over)
	newSize = uint32(GM1TILEBYTES)
	return 
}


func (this *Image) ReplaceTGX(img image.Image, palette *TGXPalette, header *GM1ImageHeader, tilePosition int) (newSize uint32){
	// type GM1ImageHeader struct {
	// Width            uint16
	// Height           uint16
	// PositionX        uint16
	// PositionY        uint16
	// Part             byte   //tile only
	// Parts            byte   //tile only
	// TilePositionY    uint16 //tile only
	// Direction        byte
	// HorizontalOffset byte //tile only
	// DrawingBoxWdith  byte //tile only
	// PerformanceID    byte
	// part :=header.Part
	parts :=header.Parts
	// b:= img.Bounds()
	
	// h:= b.Max.Y - b.Min.Y
	edge := int(math.Sqrt(float64(parts)))
	var x,y,iw,ih int
	x=0
	y=0
	iw=0
	ih=int(header.Height)
	
	row, col, _:= header.getTileRowColDir(false)
	var p image.Point
	p.Y = y+ih -8
	switch(tilePosition){
		case TILE_LEFT:
			x = (int(int(math.Abs(float64(edge-row)))/2)+ col)*GM1TILEWIDTH
			iw = 15
			p.X = x
						
		case TILE_RIGHT:
			x = (int(int(math.Abs(float64(edge-row)))/2)+ col)*GM1TILEWIDTH + 15
			iw = 15
			p.X = 15
			
		case TILE_TOP:		
			x = (int(int(math.Abs(float64(edge-row)))/2)+ col)*GM1TILEWIDTH
			iw = GM1TILEWIDTH
			p.X = x
	}
	
	y = 0
	
	rect := image.Rect(x, y, iw, ih)
	
	draw.DrawMask(this.data, rect, img, image.ZP, &tileInvMask{p}, image.ZP, draw.Over)
	//draw.Draw(this, rect, tileImg.data, image.ZP, draw.Over)
	newSize = this.countSizeTGX()
	return 
}

func (this *Image) countSizeTGX()(size uint32){
	//@todo
	bnd := this.data.Bounds()
	maxX:= bnd.Max.X
	maxY := bnd.Max.Y
	var r,g,b,a uint32
	var _r,_g,_b uint32
	// var color Color
	
	var count uint32 = 0
	
	var prev,cur int
	size = 0
	bFirst :=true
	for y:=0; y<maxY; y++{
		if y!=0{
			size++
		}
		for x:=0; x<maxX; x++{
			r,g,b,a = this.data.At(x,y).RGBA()
			if (a<255){
				cur = TGX_TRANSPARENT				
			}else{
				cur = TGX_STREAM				
			}
			
			if ((cur== TGX_STREAM) || (cur==TGX_REPEAT)) && (!bFirst) && (r == _r)&& (g == _g) && (b == _b){
				cur = TGX_REPEAT
			}
			
			count ++
			
			if (!bFirst) && (cur!=prev){
				size+=2
				if (prev==TGX_STREAM){
					size+=count;
				}
				count = 0
			}
			
			prev = cur
			
			_r = r
			_g = g
			_b = b
			if bFirst{
				bFirst = false
			}
		}
	}
	return
}

type tileMask struct {
    p image.Point	
}

func (t *tileMask) ColorModel() color.Model {
    return color.AlphaModel
}

func (t *tileMask) Bounds() image.Rectangle {
    return image.Rect(t.p.X, t.p.Y, t.p.X+GM1TILEWIDTH, t.p.Y+GM1TILEHEIGHT)
}

func (t *tileMask) At(x, y int) color.Color {
	if (x<(t.p.X + 15-GM1TilePixelsPerLine[y]/2)) || (x>( t.p.X + 15+GM1TilePixelsPerLine[y])){
		return color.Alpha{255}
	}
   
    return color.Alpha{0}
}

type tileInvMask struct {
    p image.Point	
}

func (t *tileInvMask) ColorModel() color.Model {
    return color.AlphaModel
}

func (t *tileInvMask) Bounds() image.Rectangle {
    return image.Rect(t.p.X, t.p.Y, t.p.X+GM1TILEWIDTH, t.p.Y+GM1TILEHEIGHT)
}

func (t *tileInvMask) At(x, y int) color.Color {
	if (x<(t.p.X + 15-GM1TilePixelsPerLine[y]/2)) || (x>( t.p.X + 15+GM1TilePixelsPerLine[y])){
		return color.Alpha{0}
	}
   
    return color.Alpha{255}
}
