package main

import "testing"

func TestBitExtract(t *testing.T) {
	red := byte(7)
	rede := byte(extractBitsetValue(int(red), 0, 3))

	if red != rede {
		t.Error("Error: ", red, "!=", rede)
	}

	//bit testing
	testBit := byte(32) //bit 5
	if bitTest(int(testBit), 6) == true {
		t.Error("Bittest failed")
	}
	if bitTest(int(testBit), 5) == false {
		t.Error("Bittest failed")
	}
	if bitTest(int(testBit), 4) == true {
		t.Error("Bittest failed")
	}
}
