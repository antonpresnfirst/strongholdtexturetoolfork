package main

func pow(x int, exp int) int {
	r := 1
	for i := 0; i < exp; i++ {
		r *= x
	}
	return r
}

func bitTest(x int, pos uint) bool {
	return (x & (1 << pos)) > 0
}

func extractBitsetValue(data int, start, count int) (r int) {
	exp := 0
	for i := start; i < start+count; i++ {
		//test bit
		if bitTest(data, uint(i)) {
			r += pow(2, exp)
		}
		exp++
	}
	return
}

func writeBitsetValue(bits byte, start, count int, data *uint16)  {
	exp := 0
	var r int
	for i := 0; i< start; i++ {
		if bitTest(int(*data), uint(i)) {
			r += pow(2,exp)
		}
		exp ++
	}
	for i := start; i < start+count; i++ {
		//test bit
		if bitTest(int(bits), uint(i)) {
			r += pow(2, exp)
		}
		exp++
	}
	for i := start+count; i < 16; i++ {
		//test bit
		if bitTest(int(*data), uint(i)) {
			r += pow(2, exp)
		}
		exp++
	}
	
	*data = uint16(r)
	
}
