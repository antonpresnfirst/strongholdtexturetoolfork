package main

import (
	"encoding/binary"
	"image"
	"image/draw"
	"image/png"
	"log"
	"math"
	"os"
	"path"
	"strconv"
	// "strings"
	"regexp"
	"fmt"
)

/*
GM1 File:
	File Header
	Color Pallet
	Image Offset List
	Image Size List
	Image Header List
	Image Data List
*/

const (
	TYPE_TGX          = iota + 1 //interface items, building animations (no file header!)
	TYPE_ANIMATION               //animations
	TYPE_TGXTILE                 //Stored in tile format
	TYPE_TGXFONT                 //Font
	TYPE_BITMAP                  //Walls, grass, stones and more (no compression, 16bit per pixel)
	TYPE_TGXCONSTSIZE            //All images have the same size
	TYPE_BITMAPOTHER             //not realy used... (no compression)
)

const (
	GM1TILEBYTES  = 512
	GM1TILEWIDTH  = 30
	GM1TILEHEIGHT = 16
)

const (
	DIRECTION_NONE = iota
	DIRECTION_DOWN
	DIRECTION_RIGHT
	DIRECTION_LEFT
)

const (
	TILE_LEFT = iota
	TILE_RIGHT
	TILE_TOP
)

var GM1TilePixelsPerLine = [...]int{2, 6, 10, 14, 18, 22, 26, 30, 30, 26, 22, 18, 14, 10, 6, 2} //Tiles are 30x16 px
var PathTypeTable = [...]string{"nopath", "tgx", "animations", "tile", "font", "bitmap", "tgxconst", "bitmapother"}

type GM1Header struct {
	u0         [3]int32
	ImageCount int32
	u1         int32
	DataType   int32
	u2         [2]int32
	SizeType   int32
	u3         [11]int32
	DataSize   int32 //size of the file left to read in bytes
	u4         int32
}

type GM1ImageHeader struct {
	Width            uint16
	Height           uint16
	PositionX        uint16
	PositionY        uint16
	Part             byte   //tile only
	Parts            byte   //tile only
	TilePositionY    uint16 //tile only
	Direction        byte
	HorizontalOffset byte //tile only
	DrawingBoxWdith  byte //tile only
	PerformanceID    byte
}

type GM1EntryInfo struct {
	Offset uint32
	Size   uint32
	Header GM1ImageHeader
	Image  Image
	Tile   Image
}

func (this *GM1Header) Read(reader *os.File) {
	binary.Read(reader, binary.LittleEndian, &this.u0)
	binary.Read(reader, binary.LittleEndian, &this.ImageCount)
	binary.Read(reader, binary.LittleEndian, &this.u1)
	binary.Read(reader, binary.LittleEndian, &this.DataType)
	binary.Read(reader, binary.LittleEndian, &this.u2)
	binary.Read(reader, binary.LittleEndian, &this.SizeType)
	binary.Read(reader, binary.LittleEndian, &this.u3)
	binary.Read(reader, binary.LittleEndian, &this.DataSize)
	binary.Read(reader, binary.LittleEndian, &this.u4)
}

func (this *GM1ImageHeader) Read(reader *os.File) {
	binary.Read(reader, binary.LittleEndian, &this.Width)
	binary.Read(reader, binary.LittleEndian, &this.Height)
	binary.Read(reader, binary.LittleEndian, &this.PositionX)
	binary.Read(reader, binary.LittleEndian, &this.PositionY)
	binary.Read(reader, binary.LittleEndian, &this.Part)
	binary.Read(reader, binary.LittleEndian, &this.Parts)
	binary.Read(reader, binary.LittleEndian, &this.TilePositionY)
	binary.Read(reader, binary.LittleEndian, &this.Direction)
	binary.Read(reader, binary.LittleEndian, &this.HorizontalOffset)
	binary.Read(reader, binary.LittleEndian, &this.DrawingBoxWdith)
	binary.Read(reader, binary.LittleEndian, &this.PerformanceID)
}

func (this *GM1Header) Write(writer *os.File) {
	binary.Write(writer, binary.LittleEndian, &this.u0)
	binary.Write(writer, binary.LittleEndian, &this.ImageCount)
	binary.Write(writer, binary.LittleEndian, &this.u1)
	binary.Write(writer, binary.LittleEndian, &this.DataType)
	binary.Write(writer, binary.LittleEndian, &this.u2)
	binary.Write(writer, binary.LittleEndian, &this.SizeType)
	binary.Write(writer, binary.LittleEndian, &this.u3)
	binary.Write(writer, binary.LittleEndian, &this.DataSize)
	binary.Write(writer, binary.LittleEndian, &this.u4)
}

func (this *GM1ImageHeader) Write(writer *os.File) {
	binary.Write(writer, binary.LittleEndian, &this.Width)
	binary.Write(writer, binary.LittleEndian, &this.Height)
	binary.Write(writer, binary.LittleEndian, &this.PositionX)
	binary.Write(writer, binary.LittleEndian, &this.PositionY)
	binary.Write(writer, binary.LittleEndian, &this.Part)
	binary.Write(writer, binary.LittleEndian, &this.Parts)
	binary.Write(writer, binary.LittleEndian, &this.TilePositionY)
	binary.Write(writer, binary.LittleEndian, &this.Direction)
	binary.Write(writer, binary.LittleEndian, &this.HorizontalOffset)
	binary.Write(writer, binary.LittleEndian, &this.DrawingBoxWdith)
	binary.Write(writer, binary.LittleEndian, &this.PerformanceID)
}

type GM1Image struct {
	filename string
	entries  []GM1EntryInfo
	header   GM1Header
	palette  TGXPalette
	collections [][]GM1EntryInfo
}

func (this *GM1Image) Load(filename string) {
	this.filename = filename

	file, err := os.Open(filename)
	if err != nil {
		log.Fatal("Load: cannot open file", err)
	}

	//Read header
	this.header.Read(file)

	//Read Palette
	this.palette.Read(file)

	//Read entry data
	this.entries = make([]GM1EntryInfo, this.header.ImageCount)
	//offsets
	for i, _ := range this.entries {
		binary.Read(file, binary.LittleEndian, &this.entries[i].Offset)
	}
	//sizes
	for i, _ := range this.entries {
		binary.Read(file, binary.LittleEndian, &this.entries[i].Size)
	}

	//Read headers
	for i, _ := range this.entries {
		this.entries[i].Header.Read(file)
	}

	endOfHeader, _ := file.Seek(0, 1) //all offsets are relative to this

	//Read images
	log.Println("GM1: Type: ", PathTypeTable[this.header.DataType], " File:", filename)
	var k int
	k = 0
	for i, entry := range this.entries {
		//seek to position
		file.Seek(int64(entry.Offset)+endOfHeader, 0)

		switch this.header.DataType {
		case TYPE_BITMAP:
			logVerbose("GM1: Bitmap Width", entry.Header.Width, " Height: ", entry.Header.Height)
			this.entries[i].Image.DecodeBitmap(entry.Header, endOfHeader+int64(entry.Offset), int64(entry.Size), file)

		case TYPE_TGX, TYPE_TGXFONT, TYPE_TGXCONSTSIZE:
			logVerbose("GM1: TGX Width", entry.Header.Width, " Height: ", entry.Header.Height)
			this.entries[i].Image.DecodeTGX(TGXHeader{Width: entry.Header.Width, Height: entry.Header.Height}, endOfHeader+int64(entry.Offset), int64(entry.Size), file, nil)

		case TYPE_ANIMATION:
			logVerbose("GM1: TGX (animated) Width", entry.Header.Width, " Height: ", entry.Header.Height)
			this.entries[i].Image.DecodeTGX(TGXHeader{Width: entry.Header.Width, Height: entry.Header.Height}, endOfHeader+int64(entry.Offset), int64(entry.Size), file, &this.palette)

		case TYPE_TGXTILE:
			logVerbose("GM1: Tile")
			if (this.entries[i].Header.Parts-1) == this.entries[i].Header.Part{
				k++;
				logVerbose(fmt.Sprintf("Collection start: %d (offset: %d, size: %d)\n",k, entry.Offset, entry.Size));
				logVerbose(this.entries[i]);
			}
			this.entries[i].Tile.DecodeTile(endOfHeader+int64(entry.Offset), file)
			this.entries[i].Image.DecodeTGX(TGXHeader{Width: uint16(entry.Header.DrawingBoxWdith), Height: entry.Header.TilePositionY + GM1TILEHEIGHT}, endOfHeader+int64(entry.Offset)+GM1TILEBYTES, int64(entry.Size)-GM1TILEBYTES, file, nil)

		default:
			log.Println("Unhandled Type")
		}
	}
}

func (this *GM1Image) Save() {
	//create output dir
	_, filename := path.Split(this.filename)
	os.MkdirAll("output/"+PathTypeTable[int(this.header.DataType)]+"/"+filename, os.ModeDir|os.ModePerm)

	for i, entry := range this.entries {
		savePath := "output/" + PathTypeTable[int(this.header.DataType)] + "/" + filename + "/" + strconv.Itoa(int(entry.Header.Part)) + "_" + strconv.Itoa(int(entry.Header.Parts)) + "img" + strconv.Itoa(i)
		entry.Image.Save(savePath, "png")
		entry.Tile.Save(savePath+"_TILE", "png")
	}
}
func (this *GM1Image) SaveBmp() {
	//create output dir
	_, filename := path.Split(this.filename)
	os.MkdirAll("output/"+PathTypeTable[int(this.header.DataType)]+"/"+filename, os.ModeDir|os.ModePerm)

	for i, entry := range this.entries {
		savePath := "output/" + PathTypeTable[int(this.header.DataType)] + "/" + filename + "/" + strconv.Itoa(int(entry.Header.Part)) + "_" + strconv.Itoa(int(entry.Header.Parts)) + "img" + strconv.Itoa(i)
		entry.Image.Save(savePath, "bmp")
		entry.Tile.Save(savePath+"_TILE", "bmp")
	}
}


func (this *GM1Image) SaveCollection() {

	//create collections
	var collections [][]GM1EntryInfo

	collectionIndex := 0
	i := 0

	for _, entry := range this.entries {
		//new collection
		if entry.Header.Part == 0 {
			logVerbose("New Collection: Parts:", entry.Header.Parts)
			collections = append(collections, make([]GM1EntryInfo, entry.Header.Parts))
			i = 0
		}

		if entry.Header.Parts == 0 {
			continue
		}

		//insert image to collection
		collections[collectionIndex][i] = entry
		i++

		//end of collection
		if entry.Header.Part == entry.Header.Parts-1 {
			logVerbose("End of Collection")
			collectionIndex++
		}
	}

	//build final images
	for c, collection := range collections {
		offsetX := math.MaxInt32
		offsetY := math.MaxInt32
		maxX := 0
		maxY := 0

		//find bounds
		for i := 0; i < len(collection); i++ {
			x := int(collection[i].Header.PositionX)
			y := int(collection[i].Header.PositionY)
			if offsetX > x {
				offsetX = x
			}

			if maxX < x {
				maxX = x
			}

			if offsetY > y {
				offsetY = y
			}

			if maxY < y {
				maxY = y + int(collection[i].Header.TilePositionY)
			}
		}

		//create image
		img := image.NewRGBA(image.Rect(0, 0, maxX-offsetX+GM1TILEWIDTH, maxY-offsetY+GM1TILEHEIGHT))

		//paint entities
		for i := 0; i < len(collection); i++ {
			x := int(collection[i].Header.PositionX) - offsetX
			y := int(collection[i].Header.PositionY+collection[i].Header.TilePositionY) - offsetY
			rect := image.Rect(x, y, x+GM1TILEWIDTH, y+GM1TILEHEIGHT)
			draw.Draw(img, rect, collection[i].Tile.data, image.ZP, draw.Over)

			x = int(collection[i].Header.PositionX+uint16(collection[i].Header.HorizontalOffset)) - offsetX
			y = int(collection[i].Header.PositionY) - offsetY
			rect = image.Rect(x, y, x+int(collection[i].Header.Width), y+int(collection[i].Header.Height))
			draw.Draw(img, rect, collection[i].Image.data, image.ZP, draw.Over)
			draw.Draw(img, rect, collection[i].Image.data, image.ZP, draw.Over)

		}

		_, filename := path.Split(this.filename)
		os.MkdirAll("output/"+PathTypeTable[int(this.header.DataType)]+"/"+filename, os.ModeDir|os.ModePerm)
		savePath := "output/" + PathTypeTable[int(this.header.DataType)] + "/" + filename + "/" + "collection" + strconv.Itoa(c)
		file, _ := os.Create(savePath + ".png")
		png.Encode(file, img)
		file.Close()
	}
}

func (this *GM1Image) MakeCollections() {

	collectionIndex := 0
	i := 0

	for _, entry := range this.entries {
		//new collection
		if entry.Header.Part == 0 {
			logVerbose("New Collection: Parts:", entry.Header.Parts)
			this.collections = append(this.collections, make([]GM1EntryInfo, entry.Header.Parts))
			i = 0
		}

		if entry.Header.Parts == 0 {
			continue
		}

		//insert image to collection
		this.collections[collectionIndex][i] = entry
		i++

		//end of collection
		if entry.Header.Part == entry.Header.Parts-1 {
			logVerbose("End of Collection")
			collectionIndex++
		}
	}

}


func (this *GM1Image) SaveTileset() {
	numberOfEntries := len(this.entries)
	tilesPerRow := int(math.Sqrt(float64(numberOfEntries))) + 1

	var x, y int

	//create image
	img := image.NewRGBA(image.Rect(0, 0, tilesPerRow*GM1TILEWIDTH, tilesPerRow*GM1TILEHEIGHT))

	for i := 0; i < numberOfEntries; i++ {
		rect := image.Rect(x*GM1TILEWIDTH, y*GM1TILEHEIGHT, (x+1)*GM1TILEWIDTH, (y+1)*GM1TILEHEIGHT)
		draw.Draw(img, rect, this.entries[i].Tile.data, image.ZP, draw.Over)
		x++
		if x > tilesPerRow {
			x = 0
			y++
		}
	}

	_, filename := path.Split(this.filename)
	os.MkdirAll("output/"+PathTypeTable[int(this.header.DataType)]+"/"+filename, os.ModeDir|os.ModePerm)
	savePath := "output/" + PathTypeTable[int(this.header.DataType)] + "/" + filename + "/" + "tileset"
	file, _ := os.Create(savePath + ".png")
	png.Encode(file, img)
	file.Close()
}

func (this *GM1Image) SmartSave() {
	if this.header.DataType == TYPE_TGXTILE {
		if build {
			this.SaveCollection()
		}
		if buildTileset {
			this.SaveTileset()
		}
	} else {
		this.SaveBmp()
	}
}

func (this *GM1Image) Write(filename string) {
	
	file, err := os.Create(filename)
	
	if err != nil {
		log.Fatal("Write: cannot create file", err)
	}
	
	pos,_ :=file.Seek(0,1)
	logVerbose(fmt.Sprintf("header offset = %d",pos))
	this.header.Write(file)

	pos,_ =file.Seek(0,1)
	logVerbose(fmt.Sprintf("palette offset = %d",pos))
	this.palette.Write(file)

	pos,_ =file.Seek(0,1)
	logVerbose(fmt.Sprintf("entries offsets offset = %d",pos))
	//offsets
	for i, _ := range this.entries {
		binary.Write(file, binary.LittleEndian, this.entries[i].Offset)
	}
	//sizes
	pos,_ =file.Seek(0,1)
	logVerbose(fmt.Sprintf("entries sizes offset = %d",pos))
	for i, _ := range this.entries {
		binary.Write(file, binary.LittleEndian, this.entries[i].Size)
	}

	//write headers
	pos,_ =file.Seek(0,1)
	logVerbose(fmt.Sprintf("headers offset = %d",pos))
	for i, _ := range this.entries {
		this.entries[i].Header.Write(file)
	}

	// endOfHeader, _ := file.Seek(0, 1) //all offsets are relative to this
	
	// //Read images
	// log.Println("GM1: Type: ", PathTypeTable[this.header.DataType], " File:", filename)
	endOfHeader,_ :=file.Seek(0,1)
	logVerbose(fmt.Sprintf("endOfHeader offset = %d",endOfHeader))
	
	maxX :=0
	maxY :=0
	col :=0
	//row :=0
	//h:= 0
	//w := 0
	var isize image.Point
	var dir byte
	for i, _ := range this.entries {
		//seek to position
		//file.Seek(int64(entry.Offset)+endOfHeader, 0)

		switch this.header.DataType {
	/* 	case TYPE_BITMAP:
			logVerbose("GM1: Bitmap Width", entry.Header.Width, " Height: ", entry.Header.Height)
			this.entries[i].Image.DecodeBitmap(entry.Header, endOfHeader+int64(entry.Offset), int64(entry.Size), file)

		case TYPE_TGX, TYPE_TGXFONT, TYPE_TGXCONSTSIZE:
			logVerbose("GM1: TGX Width", entry.Header.Width, " Height: ", entry.Header.Height)
			this.entries[i].Image.DecodeTGX(TGXHeader{Width: entry.Header.Width, Height: entry.Header.Height}, endOfHeader+int64(entry.Offset), int64(entry.Size), file, nil)

		case TYPE_ANIMATION:
			logVerbose("GM1: TGX (animated) Width", entry.Header.Width, " Height: ", entry.Header.Height)
			this.entries[i].Image.DecodeTGX(TGXHeader{Width: entry.Header.Width, Height: entry.Header.Height}, endOfHeader+int64(entry.Offset), int64(entry.Size), file, &this.palette)
 */
		case TYPE_TGXTILE:
			logVerbose(fmt.Sprintf("GM1: Tile (collection: %d)", i))
			
			this.entries[i].Tile.EncodeTile( file)
			_, col, dir = this.entries[i].Header.getTileRowColDir(true)
			logVerbose(fmt.Sprintf("col row dir c: %d, r: %d, d: %d",col,-1, dir));
			//h, w = this.entries[i].Header.getTileCollectionHeightWidth()
			isize  = this.entries[i].Image.data.Bounds().Size()
			switch(dir){
				case DIRECTION_DOWN:
					maxX = int(GM1TILEWIDTH)				
					maxY = int(isize.Y - int(GM1TILEHEIGHT/2)*col)
				case DIRECTION_LEFT, DIRECTION_RIGHT:
					maxX = int(GM1TILEWIDTH/2) + 1
					maxY = int(isize.Y - int(GM1TILEHEIGHT/2)*col)
				case DIRECTION_NONE:
				default:
					dir = DIRECTION_NONE
			}
			
			if dir != DIRECTION_NONE {
				logVerbose(fmt.Sprintf("tile size w: %d, h: %d",maxX,maxY))
				// optimization for 1 tile 
				//if (this.entries[i].Header.Parts == 1) {
					//maxY += 7
				//}
				logVerbose(this.entries[i]);
				this.entries[i].Offset, this.entries[i].Size = this.entries[i].Image.EncodeTGX(file, nil, maxX, maxY)
				//TGXHeader{Width: uint16(entry.Header.DrawingBoxWdith), Height: entry.Header.TilePositionY + GM1TILEHEIGHT}, int64(entry.Size)-GM1TILEBYTES, 	
			}
			
			
			this.entries[i].Size += GM1TILEBYTES
			this.entries[i].Offset -= uint32(endOfHeader) + GM1TILEBYTES
			logVerbose(fmt.Sprintf("written at offset = %d, size = %d",this.entries[i].Offset,this.entries[i].Size))
		default:
			log.Println("Unhandled Type")
		}
	}
	
	//rewind to write new offsets
	pos,_ =file.Seek(0,0)
	logVerbose(fmt.Sprintf("header offset = %d",pos))
	this.header.Write(file)

	pos,_ =file.Seek(0,1)
	logVerbose(fmt.Sprintf("palette offset = %d",pos))
	this.palette.Write(file)

	pos,_ =file.Seek(0,1)
	logVerbose(fmt.Sprintf("entries offsets offset = %d",pos))
	//offsets
	for i, _ := range this.entries {
		binary.Write(file, binary.LittleEndian, this.entries[i].Offset)
	}
	//sizes
	pos,_ =file.Seek(0,1)
	logVerbose(fmt.Sprintf("entries sizes offset = %d",pos))
	for i, _ := range this.entries {
		binary.Write(file, binary.LittleEndian, this.entries[i].Size)
	}

	//write headers
	pos,_ =file.Seek(0,1)
	logVerbose(fmt.Sprintf("headers offset = %d",pos))
	for i, _ := range this.entries {
		this.entries[i].Header.Write(file)
	}
	file.Close()
}

func (this * GM1Image)InjectCollection(filename string) {
	file, err := os.Open(filename);
	if err != nil {
		log.Fatal("Load: cannot open file", err)
	}

	r, _ := regexp.Compile(`^.*?collection(\d+)\..*?$`);
	match := r.FindStringSubmatch(filename)
	
	num, err := strconv.Atoi(match[1])
	
	if err != nil {
		log.Fatal("InjectCollection: cannot convert collection number")
	}
	log.Println(num)
	if this.collections[num]==nil {
		log.Fatal("InjectCollection: no such collection")
	}
	
	parts := len(this.collections[num])
	log.Println(parts)
	collection := this.collections[num]
	// maxnum := uint16(math.Sqrt(float64(parts)))
	// width := collection[0].Header.Width*maxnum+(maxnum-1)*2
	// height := collection[0].Header.Height*maxnum
	// log.Println(maxnum)
	// log.Println("printing entries")
	// log.Println(entries)
	// log.Println("printing entries end")
	offsetX, offsetY, maxX, maxY := this.FindCollectionBounds(collection)
	width := maxX-offsetX+GM1TILEWIDTH
	height := maxY-offsetY+GM1TILEHEIGHT
	
	conf, _, err := image.DecodeConfig(file)
	if (conf.Width!=width) || (conf.Height!=height) {
		log.Println(conf.Width)
		log.Println(conf.Height)
		log.Println(width)
		log.Println(height)
		log.Fatal("InjectCollection: width or height not match")
	}
	file.Seek(0, 0)
	img, err := png.Decode(file)
	if err != nil {
		log.Fatal("InjectCollection: cannot decode png image")
	}
	bEdgesGot  := false
	bLeft := true
	// bTop := false
	bRight := false
	tilePosition := TILE_LEFT
	lenCollection := len(collection)
	numEntry := this.getNumEntryOffset(num)
	var offsetCurrent,TileSize,ImageSize uint32
	TileSize = 0
	ImageSize = 0
	offsetCurrent = 0
	offsetBefore := this.entries[numEntry+lenCollection-1].Offset
	logVerbose(fmt.Sprintf("offset before injection: %d\n",offsetBefore));
	
	var aEdges []int
	j:=0
	for i:=0; i< lenCollection; i++ {
		entry :=collection[i]
		if offsetCurrent>0{
			this.entries[numEntry].Offset = offsetCurrent
		}
		if !bEdgesGot{
			aEdges = this.getTileEdges(int(entry.Header.Parts))
			log.Println(aEdges)
			log.Println(len(aEdges))
			bEdgesGot = true
			j=0
		}
		TileSize = 0
		ImageSize = 0
		// here save image first, then save tile
		if bEdgesGot && (i==(aEdges[j])){
			if (i==lenCollection){
				// bTop = true
				bLeft = false
				bRight = false
				tilePosition = TILE_TOP
			} else if (bLeft){
				tilePosition = TILE_LEFT
			} else if (bRight){
				tilePosition = TILE_RIGHT
			}
			
			bLeft = !bLeft
			bRight = !bLeft
			
			TileSize = this.entries[numEntry].Tile.ReplaceTile(img, &entry.Header)
			ImageSize = this.entries[numEntry].Image.ReplaceTGX(img, nil, &entry.Header, tilePosition)
			
			j++
		} else {
		// here save tile only
			
			TileSize = this.entries[numEntry].Tile.ReplaceTile( img, &entry.Header)
		}
		this.entries[numEntry].Size = TileSize + ImageSize
		offsetCurrent = this.entries[numEntry].Offset + this.entries[numEntry].Size
		numEntry++
		
	}
	deltaOffset := this.entries[numEntry-1].Offset - offsetBefore
	logVerbose(fmt.Sprintf("delta offset after injection: %d\n",deltaOffset));
	if deltaOffset >0 {
		this.recountOffsetsAfter(numEntry,deltaOffset)
	}
	log.Println("InjectCollection: working")

}

func (this * GM1Image)getNumEntryOffset(numCollection int)(numEntry int){
	numEntry = 0
	for i:=0;i<numCollection;i++{
		numEntry+=len(this.collections[i])
	}
	return
}

func (this * GM1Image)recountOffsetsAfter(numEntry int,deltaOffset uint32){
	for i:=numEntry;i<len(this.entries);i++{
		this.entries[i].Offset+=deltaOffset
	}
	this.header.DataSize += int32(deltaOffset)
}

func (this * GM1Image)SaveNew() {

	r, _ := regexp.Compile(`^.*?/(.*?)\.gm1$`);
	match := r.FindStringSubmatch(this.filename)
	
	filename := match[1]
	this.Write(filename+"_out.gm1")
	
}

func (this * GM1Image) FindCollectionBounds(collection []GM1EntryInfo)(offsetX, offsetY, maxX, maxY int){
	offsetX = math.MaxInt32
	offsetY = math.MaxInt32
	maxX = 0
	maxY = 0

	//find bounds
	for i := 0; i < len(collection); i++ {
		x := int(collection[i].Header.PositionX)
		y := int(collection[i].Header.PositionY)
		if offsetX > x {
			offsetX = x
		}

		if maxX < x {
			maxX = x
		}

		if offsetY > y {
			offsetY = y
		}

		if maxY < y {
			maxY = y + int(collection[i].Header.TilePositionY)
		}
	}
	return
}

func (this * GM1Image) getTileEdges(partsCount int)(aEdges [] int) {
	edgeLen := int(math.Sqrt(float64(partsCount)))
	count:=0
	for i:=1; i<edgeLen;i++ {
		count+=i
	}
	
	k:=0
	aEdges=make([]int,edgeLen*2-1)
	for i:=edgeLen-1; i>=0; i-- {
		
		aEdges[k] = count;
		k++
		count+=i+1
		if (count<partsCount){
			aEdges[k] = count-1;
			k++
		}		
	}
	return
	
}

/**
 * find row and column for current part in the tiles collection
 * also the tile direction relative to image (right , left, down, or none)
 * i.e: in 4X4 tile collection 
		part 5 has 	col = 5 and row = 4 and direction none
		part 14 has col = 4 	row = 1 and direction left
		part 10 has col = 1		row = 2 and direction right
		part 15 has col = 3		row = 0 and direction down
		cols
		0 1 2 3 4 5 6
rows   0      15
	   1    13  14
	   2  10  11  12
	   36   7   8   9	   
	   4  3   4   5
	   5    1   2
	   6      0
	
**/
func (this * GM1ImageHeader) getTileRowColDir(invheight bool) (row,col int, dir byte){
	ns := byte(math.Sqrt(float64(this.Parts)))
	part := byte(this.Part)
	nr := ns *2 -1
	mag := byte((this.Parts/2) - (ns/2) + 1)
	partsub := mag
	i := ns-1
	
	for found:=true; found; found = partsub <= part {
		partsub = byte(math.Abs(float64(partsub - i)))
		i--
	}
	
	partstart := partsub
	kstart := 1 - i
	
	k := (part - partstart)*2 + kstart
	
	col = int(ns + k - 1)
	if invheight {
		row = int(-kstart)	
	} else {
		row = int(nr + kstart - 1)	
	}
	
	// @todo calc direction
	dir = this.Direction
	return
}

func (this * GM1ImageHeader) getTileCollectionHeightWidth () (h, w int) {
	ns := int(math.Sqrt(float64(this.Parts)))
	h = int(ns * GM1TILEHEIGHT)
	w = int(ns * GM1TILEWIDTH)
	return
}
